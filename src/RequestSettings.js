class RequestSettings {
    constructor({city, apiKey}) {
        this.city = city;
        this.apiKey = apiKey;
    }

    setCity(city) {
        this.city = city;
    }

    toString() {
        return `https://api.openweathermap.org/data/2.5/forecast?q=${this.city}&appid=${this.apiKey}`;
    }
}

module.exports.RequestSettings = RequestSettings;