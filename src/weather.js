const request = require('request');
const { DayBox } = require("./templates/Day.js");
const { CurrentDay } = require("./templates/CurrentDay.js");
const { RequestSettings } = require("./RequestSettings");

function toCelsius(k) {
    return Math.round(k - 273.15);
}

function getDayString(date, days) {
    return days[date.getDay()];
}

function getWeekWeatherTemplate(weatherIcons, weatherData) {
    let result = "";
    let date = null;
    let days = weatherData.list.filter((item) => new Date().getDate() !== new Date(item['dt_txt']).getDate());
    let daysStrings = ["s u n", "m o n", "t u e", "w e d", "t h u", "f r i", "s a t"];
    let i = 0;
    for (let val of days) {
        if (i > 3) break;
        date = new Date(val['dt_txt']);
        if (date.getHours() === 12) {
            result += new DayBox({
                imgPath: weatherIcons.get(val['weather'][0].main),
                weatherName: val['weather'][0].description,
                day: getDayString(date, daysStrings),
                temp: toCelsius(val.main.temp),
            }).getTemplate();
            i++;
        }
    }
    return result;
}

function getCurrentWeatherData(weatherData) {
    let weathers = weatherData.list;
    let date = new Date();
    let currentWeather = null;

    for (let i = 0; i < weathers.length; i++) {
        if (date.getHours() >= new Date(weathers[i]['dt_txt']).getHours()) {
            currentWeather = weathers[i];
            break;
        }
    }
    return { currentWeather, date };
}

function getCurrentWeatherTemplate(weatherData, weatherIcons) {
    const { currentWeather, date } = getCurrentWeatherData(weatherData);

    const template = new CurrentDay({
        imgPath: weatherIcons.get(currentWeather['weather'][0].main),
        weatherName: currentWeather['weather'][0].description,
        temp: toCelsius(currentWeather['main'].temp),
        date: date,
        days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        geo: weatherData.city,
    });
    return template.getTemplate();
}

function showTemplate(id, template) {
    let parent = document.getElementById(id);
    parent.innerHTML = template;
    return showTemplate;
}

function makeRequest(url) {
    request(url, function (error, res, body) {
        let weatherData = JSON.parse(body);
        let weatherIcons = new Map([
            ["Clouds", "./img/cloudsW.png"],
            ["Clear", "./img/clearW.png"],
            ["Snow", "./img/snowBig.png"],
            ["Rain", "./img/rainW.png"],
            ["Thunderstorm", "./img/thunderW.png"]
        ]);
        let template = getWeekWeatherTemplate(weatherIcons, weatherData);
        let template2 = getCurrentWeatherTemplate(weatherData, weatherIcons);
        showTemplate("days", template)("curr-day", template2);
    });
}

function changeTheme() {
    let timeImage = document.getElementById("time-image");
    let hours = new Date().getHours();

    if (hours >= 0 && hours <= 6) {
        timeImage.classList.add("image_night");
        document.body.classList.add("night");
    }
    if (hours >= 6 && hours <= 12) {
        timeImage.classList.add("image_morning");
        document.body.classList.add("default");
    } else if (hours >= 12 && hours <= 18) {
        timeImage.classList.add("image_day");
        document.body.classList.add("default");
    } else if (hours >= 18 && hours <= 23) {
        timeImage.classList.add("image_evening");
        document.body.classList.add("night");
    }
}

function init() {
    let settings = new RequestSettings({
        city : "Minsk",
        apiKey: "df918ce6a0f3fcd5288b1272be1bf342"
    });

    let searchButton = document.getElementById('search');

    searchButton.addEventListener("click", function() {
        let city = document.getElementById("city").value;
        settings.setCity(city);
        makeRequest(settings.toString());
    });

    changeTheme();
    makeRequest(settings.toString());
    setInterval(changeTheme, 1000*60*6);
}

init();

