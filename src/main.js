const {app, BrowserWindow} = require("electron");

function createWindow() {
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        icon: `${__dirname}/img/weather.png`,
        hasShadow: true,
        frame: false,
        webPreferences: {
            nodeIntegration: true
        }
    });

    win.loadURL(`file://${__dirname}/index.html`);
    win.on("closed", () => {
        win = null;
    });
    win.webContents.openDevTools()
}

app.on("ready", createWindow);
