const remote = require('electron').remote;

const closeButton = document.getElementById("close");
const resizeButton = document.getElementById("resize");

resizeButton.addEventListener("click", function () {
    let window = remote.getCurrentWindow();
    if (!window.isMaximized()) {
        window.maximize();
    } else {
        window.unmaximize();
    }
})

closeButton.addEventListener("click", function () {
    let window = remote.getCurrentWindow();
    window.close();
});