const lookup = require('country-code-lookup');
const { Template } = require("./Template.js");

class CurrentDay extends Template{
    constructor(options) {
        super(options);
    }

    getTemplate() {
        return `
            <div class="row row_jc-c">
                <div class="weather-box weather-box_current-day column column_ai-c">
                    <img src=${this.options.imgPath} width="90" alt=${this.options.weatherName}>
                    <span>${this.options.weatherName}</span>
                </div>
                <div class="temp temp_current-day">
                    ${this.options.temp}
                </div>
            </div>
            <div class="current-date">
                <span id="current-day">${this.options.days[this.options.date.getDay()]}, </span>
                <span id="current-date">
                    ${this.options.date.getDate()} ${this.options.months[this.options.date.getMonth()]} ${this.options.date.getFullYear()}
                </span>
            </div>
            <div class="current-geo">
                ${this.options.geo.name}, ${lookup.byIso(this.options.geo.country).country}
            </div>
        `
    }
}

module.exports.CurrentDay = CurrentDay;