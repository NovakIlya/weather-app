const {Template} = require("./Template.js");

class DayBox extends Template{
    constructor(options) {
        super(options);
    }

    getTemplate() {
        return `
             <div class="day-box row">
                <div class="weather-box column column_ai-c">
                    <img src=${this.options.imgPath} width="40" alt=${this.options.weatherName}>
                    <p class="weather-name">
                        ${this.options.weatherName}
                    </p>
                </div>
                <div class="day-name">
                    ${this.options.day}
                </div>
                <div class="temp">
                    ${this.options.temp}
                </div>
            </div>  
        `;
    }
}

module.exports.DayBox = DayBox;